/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */
export default [
  {
    path: '',
    // Relative to /src/views
    view: 'Dashboard'
  },
  {
    path: '/control-panel',
    name: 'Control Panel',
    view: 'ControlPanel'
  },
  {
    path: '/table-list',
    name: 'Table List',
    view: 'TableList'
  },
  {
    path: '/chart',
    view: 'Chart'
  },
  {
    path: '/feed',
    view: 'Feed'
  },
  // {
  //   path: '/maps',
  //   view: 'Maps'
  // },
  {
    path: '/notifications',
    view: 'Notifications'
  },
  {
    path: '/test',
    view: 'Test'
  }
]
